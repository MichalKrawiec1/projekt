namespace Pizzeria.Data.Sql.DAO;

public class Product
{

    public Product()
    {

        ProductToOrders = new List<ProductToOrder>();

    }
    
    public int ProductId{get; set;}
    public string ProductName{get; set;}
    public int ProductPrice{get; set;}
    public int ProductAmount{get; set;}
    
    
    public virtual ICollection<ProductToOrder>   ProductToOrders { get; set; }

}