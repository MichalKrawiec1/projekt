namespace Pizzeria.Data.Sql.DAO;

public class Order
{

    public Order()
    {
        
        ProductToOrders = new List<ProductToOrder>();
        
    }
    
    public int OrderId { get; set; }
    public DateTime OrderDate { get; set; }
    
    
    public int ClientId { get; set; } 
    public int EmployeeId { get; set; }
    
    public virtual Employee Employee { get; set; }
    public virtual Client Client { get; set; }
    
   
    
    public virtual ICollection<ProductToOrder> ProductToOrders  { get; set; }


}