namespace Pizzeria.Data.Sql.DAO;

public class Employee
{

    public Employee()
    {

        Orders = new List<Order>();

    }
    
    public int EmployeeId{get; set;}
    public string EmployeeFirstName{get; set;}
    public string EmployeeLastName{get; set;}
    public long EmployeePesel{get; set;}
    
   
    
    public virtual ICollection<Order> Orders { get; set; }

}