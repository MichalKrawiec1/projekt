using Pizzeria.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Pizzeria.Data.Sql.DAOConfigurations
{
    public class ProductToOrderConfiguration : IEntityTypeConfiguration<ProductToOrder>
    {
        public void Configure(EntityTypeBuilder<ProductToOrder> builder)
        {
            builder.Property(c => c.ProductToOrderId).IsRequired();

            builder.Property(c => c.ProductId).IsRequired();

           
            
            builder.HasOne(x => x.Product)
                .WithMany(x => x.ProductToOrders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ProductId);
            
            

            builder.ToTable("ProductToOrder");
        }
    }
}