using Pizzeria.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Pizzeria.Data.Sql.DAOConfigurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(c => c.EmployeeId).IsRequired();
            builder.Property(c => c.EmployeeFirstName).IsRequired();
            builder.Property(c => c.EmployeeLastName).IsRequired();
            builder.Property(c => c.EmployeePesel).IsRequired();
            builder.ToTable("Employee");
        }
    }
}