using System;
using System.Collections.Generic;
using System.Linq;
using Org.BouncyCastle.Crypto.Tls;
using Pizzeria.Data.Sql.DAO;



namespace Pizzeria.Data.Sql.Migrations
{
    //klasa odpowiadająca za wypełnienie testowymi danymi bazę danych
    public class DatabaseSeed
    {
        private readonly PizzeriaDbContext _context;

        //wstrzyknięcie instancji klasy FoodlyDbContext poprzez konstruktor
        public DatabaseSeed(PizzeriaDbContext context)
        {
            _context = context;
        }

        //metoda odpowiadająca za uzupełnienie utworzonej bazy danych testowymi danymi
        //kolejność wywołania ma niestety znaczenie, ponieważ nie da się utworzyć rekordu
        //w bazie dnaych bez znajmości wartości klucza obcego
        //dlatego należy zacząć od uzupełniania tabel, które nie posiadają kluczy obcych
        //--OFFTOP
        //w przeciwną stronę działa ręczne usuwanie tabel z wypełnionymi danymi w bazie danych
        //należy zacząć od tabel, które posiadają klucze obce, a skończyć na tabelach, które 
        //nie posiadają
        public void Seed()
        {
            //regiony pozwalają na zwinięcie kodu w IDE
            //nie sa dobrą praktyką, kod w danej klasie/metodzie nie powinien wymagać jego zwijania
            //zastosowałem je z lenistwa nie powinno to mieć miejsca 

            #region CreateUsers

            var clientList = BuildClientList();

            _context.Client.AddRange(clientList);

            _context.SaveChanges();

            #endregion

            #region CreateEmployee

            var employeeList = BuildEmployeeList();

            _context.Employee.AddRange(employeeList);

            _context.SaveChanges();

            #endregion

            #region CreateProduct

            var productList = BuildProductList();

            _context.Product.AddRange(productList);

            _context.SaveChanges();

            #endregion
            
            
            
            
            #region CreateOrder
            
            var orderList = BuildOrderList();

            _context.Order.AddRange(orderList);

            _context.SaveChanges();

            #endregion
            
            
            
            
            #region CreateProductToOrder

            var productToOrderList = BuildProductToOrderList();

            _context.ProductToOrder.AddRange(productToOrderList);

            _context.SaveChanges();

            #endregion
            
            
        }
        

        private IEnumerable<Client> BuildClientList()
            {
                var clientList = new List<Client>();
                var client = new Client()
                {
                    ClientId = 1,
                    ClientFirstName = "Marek",
                    ClientLastName = "Rokita",
                    ClientPesel = 12312312312,
                };
                clientList.Add(client);

                var client2 = new Client()
                {
                    ClientId = 2,
                    ClientFirstName = "Darek",
                    ClientLastName = "Mokita",
                    ClientPesel = 12312312312

                };
                clientList.Add(client2);

                return clientList;
            }


            private IEnumerable<Employee> BuildEmployeeList()
            {
                var employeeList = new List<Employee>();
                var employee = new Employee()
                {
                    EmployeeId = 1,
                    EmployeeFirstName = "Rarek",
                    EmployeeLastName = "Dokita",
                    EmployeePesel = 12312312312,
                };
                employeeList.Add(employee);

                var employee2 = new Employee()
                {
                    EmployeeId = 2,
                    EmployeeFirstName = "Narek",
                    EmployeeLastName = "Gokita",
                    EmployeePesel = 12312312312

                };
                employeeList.Add(employee2);



                return employeeList;
            }

            
            private IEnumerable<Product> BuildProductList()
            {
                var productList = new List<Product>();
                var product = new Product()
                {
                    ProductId = 1,
                    ProductName = "Guseppe",
                    ProductPrice = 35,
                    ProductAmount = 3

                };
                productList.Add(product);

                var product2 = new Product()
                {
                    ProductId = 2,
                    ProductName = "Capriciosa",
                    ProductPrice = 35,
                    ProductAmount = 15

                };
                productList.Add(product2);



                return productList;
            }

            private IEnumerable<Order> BuildOrderList()
            {
                var orderList = new List<Order>();
                var aaaaa = new DateTime(2022,12,25,10,20,50);
                var order = new Order()
                {
                    OrderId = 1,
                    OrderDate = aaaaa,
                    ClientId = 1,
                    EmployeeId = 2
                 

                };
                orderList.Add(order);
                var aaaaa2 = new DateTime(2022,01,25,11,28,51);
                var order2 = new Order()
                {
                    OrderId = 2,
                    OrderDate = aaaaa2,
                    ClientId = 2,
                    EmployeeId = 1
                    

                };
                orderList.Add(order2);



                return orderList;
            }
           
            private IEnumerable<ProductToOrder> BuildProductToOrderList()
            {
                var productToOrderList = new List<ProductToOrder>();
                var productToOrder = new ProductToOrder()
                {
                    ProductToOrderId = 1,
                    ProductAmount = 1,
                    ProductId = 2,
                    OrderId = 1

                };
                productToOrderList.Add(productToOrder);

                var productToOrder2 = new ProductToOrder()
                {
                    ProductToOrderId = 2,
                    ProductAmount = 1,
                    ProductId = 2,
                    OrderId = 1

                };
                productToOrderList.Add(productToOrder2);



                return productToOrderList;
            }

            


    }

    
}