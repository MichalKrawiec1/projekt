using System;
using System.ComponentModel.DataAnnotations;


namespace Pizzeria.Api.BindingModels
{
    public class CreateClient
    {
        [Required]
        [Display(Name = "ClientId")]
        public int ClientId { get; set; }
        
        [Required]
        [Display(Name = "ClientFirstName")]
        public string ClientFirstName { get; set; }
        
        [Required]
        [Display(Name = "ClientLastName")]
        public string ClientLastName { get; set; }
        
        [Required]
        [Display(Name = "ClientPesel")]
        public long ClientPesel { get; set; }
    }
}